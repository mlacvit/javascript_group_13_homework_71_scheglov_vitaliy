
import { StepsModel } from './steps.model';

export class RecipeModel {
  constructor(
    public id: string,
    public title: string,
    public urlImage: string,
    public description: string,
    public ingredients: string,
    public steps: StepsModel
  ) {}
}

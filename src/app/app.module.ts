import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NewrecipeComponent } from './newrecipe/newrecipe.component';
import { RecipeComponent } from './recipe/recipe.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RecipeServices } from './recipe.services';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewrecipeComponent,
    RecipeComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [RecipeServices],
  bootstrap: [AppComponent]
})
export class AppModule { }

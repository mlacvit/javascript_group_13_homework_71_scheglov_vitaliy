import { Component, OnInit } from '@angular/core';
import { RecipeServices } from '../recipe.services';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.css']
})
export class RecipeComponent implements OnInit {
  recId = '';
  constructor(public service: RecipeServices, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.recId = params['id'];
      this.service.recipeOne(this.recId);
  });
  };

  getSteps(): any{
  return this.service.recipeOneInfo?.steps
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RecipeModel } from './recipe.model';
import { map, Subject, tap } from 'rxjs';


@Injectable()
export class RecipeServices {
  recipe: RecipeModel[] | null = null;
  preloader = new Subject<boolean>();
  recFetching = new Subject<boolean>();
  changeRec = new Subject<RecipeModel[]>()
  recipeOneInfo: RecipeModel | null = null;
  recDeleteUpLoading = new Subject<boolean>();
  constructor(private http: HttpClient) {}

  sendRecipe(body: RecipeModel){
    this.preloader.next(true);
    this.http.post(`https://mlacvit-10af9-default-rtdb.firebaseio.com/recipe.json`, body).pipe(
      tap(() => {
        this.preloader.next(false);
      }, error => {
        this.preloader.next(false);
      })
    ).subscribe();
  };

  getRecipe() {
    this.recFetching.next(true);
    this.http.get<{[id: string]: RecipeModel}>('https://mlacvit-10af9-default-rtdb.firebaseio.com/recipe.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const data = result[id];
          return new RecipeModel(
            id,
            data.title,
            data.urlImage,
            data.description,
            data.ingredients,
            data.steps,
            )
        })
      }))
      .subscribe((recipe: RecipeModel[]) => {
        this.recipe = recipe;
        this.changeRec.next(this.recipe.slice());
        this.recFetching.next(false);
      }, error => {
        this.recFetching.next(false);
      });
  };

  recipeOne(id: string) {
    this.recFetching.next(true)
    return this.http.get<RecipeModel>(`https://mlacvit-10af9-default-rtdb.firebaseio.com/recipe/${id}.json`)
      .pipe(map(result => {
        return new RecipeModel(
          id,
          result.title,
          result.urlImage,
          result.description,
          result.ingredients,
          result.steps
        );
      })).subscribe((recipe) =>{
        this.recipeOneInfo = recipe;
        this.recFetching.next(false);
      });
  };

  removeRecipe(id: string){
    this.recDeleteUpLoading.next(true);
    this.http.delete(`https://mlacvit-10af9-default-rtdb.firebaseio.com/recipe/${id}.json`).subscribe();
    this.getRecipe();
    this.recDeleteUpLoading.next(false);
  }


}

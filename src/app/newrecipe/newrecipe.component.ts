import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { RecipeModel } from '../recipe.model';
import { Subscription } from 'rxjs';
import { RecipeServices } from '../recipe.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-newrecipe',
  templateUrl: './newrecipe.component.html',
  styleUrls: ['./newrecipe.component.css']
})
export class NewrecipeComponent implements OnInit, OnDestroy {
  formGroupRecipe!: FormGroup;
  recipeFetchingSubscriptions!: Subscription;
  isFetching: boolean = false;

  constructor(private service: RecipeServices, private router: Router) { }

  ngOnInit(): void {
    this.formGroupRecipe = new FormGroup({
      title: new FormControl('', Validators.required),
      urlImage: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormControl('', Validators.required),
      steps: new FormArray([]),
    });

    this.recipeFetchingSubscriptions = this.service.preloader.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
  }

  fieldError(fieldName: string, errorField: string) {
    const field = this.formGroupRecipe.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorField]);
  };

  sendRecipe() {
    const id = Math.random().toString();
    const body = new RecipeModel(
      id,
      this.formGroupRecipe.value.title,
      this.formGroupRecipe.value.urlImage,
      this.formGroupRecipe.value.description,
      this.formGroupRecipe.value.ingredients,
      this.formGroupRecipe.value.steps,
    );
    this.service.sendRecipe(body);
    this.service.getRecipe();
    this.router.navigate(['/']);

  };

  addStep() {
    const step = <FormArray>this.formGroupRecipe.get('steps');
    const stepGroup = new FormGroup({
      step: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required)
    });
    step.push(stepGroup);
  }

  getStep() {
    const step = <FormArray>this.formGroupRecipe.get('steps');
    return step.controls;
  }

  ngOnDestroy(): void {
    this.recipeFetchingSubscriptions.unsubscribe();
  }
}

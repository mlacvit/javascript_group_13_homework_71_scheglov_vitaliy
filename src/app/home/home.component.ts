import { Component, OnDestroy, OnInit } from '@angular/core';
import { RecipeModel } from '../recipe.model';
import { RecipeServices } from '../recipe.services';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  recipe: RecipeModel[] | null = null;
  recChange!: Subscription;
  isFetch = false;
  isFetchRem = false;
  fetchSub!: Subscription;
  fetchSubRem!: Subscription;
  constructor(private service: RecipeServices) { }

  ngOnInit(): void {
    this.service.getRecipe();
    this.recChange = this.service.changeRec.subscribe((rec: RecipeModel[]) => {
      this.recipe = rec;
    });

    this.fetchSub = this.service.recFetching.subscribe((isfetching: boolean) => {
      this.isFetch = isfetching;
    });

    this.fetchSubRem = this.service.recDeleteUpLoading.subscribe((fetching: boolean) => {
      this.isFetchRem = fetching;
    });
  };

  remRecipe(id: string) {
    this.service.removeRecipe(id);
    this.service.getRecipe();
  }

  ngOnDestroy(): void {
    this.recChange.unsubscribe();
    this.fetchSub.unsubscribe();
    this.fetchSubRem.unsubscribe();
  }

}
